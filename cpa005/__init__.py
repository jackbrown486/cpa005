from __future__ import annotations

from datetime import date
from typing import Any, Iterable, TextIO, Type, TypeVar

from .logical_record import LogicalRecord
from .models import OriginatorInfo, Transaction
from .records import LogicalRecordA, LogicalRecordC, LogicalRecordD, LogicalRecordZ


class AFTFileWriter:

    LogicalRecordType = TypeVar("LogicalRecordType", bound=LogicalRecord)

    file_creation_no: int
    file_creation_date: date
    originator_info: OriginatorInfo

    def __init__(
        self,
        file_creation_no: int,
        file_creation_date: date,
        originator_info: OriginatorInfo,
    ):
        self.file_creation_no = file_creation_no
        self.file_creation_date = file_creation_date
        self.originator_info = originator_info

        self._credits: list[Transaction] = []
        self._credits_total = 0

        self._debits: list[Transaction] = []
        self._debits_total = 0

    def add_debits(self, debits: Iterable[Transaction]) -> None:
        for debit in debits:
            self._debits.append(debit)
            self._debits_total += debit.amount

    def add_credits(self, credits: Iterable[Transaction]) -> None:
        for credit in credits:
            self._credits.append(credit)
            self._credits_total += credit.amount

    def write(self, f: TextIO):
        records: list[LogicalRecord] = [self._make_record(1, LogicalRecordA)]

        for credit in self._credits:
            records.append(
                self._make_record(len(records) + 1, LogicalRecordC, **credit.dict())
            )

        for debit in self._debits:
            records.append(
                self._make_record(len(records) + 1, LogicalRecordD, **debit.dict())
            )

        records.append(
            self._make_record(
                len(records) + 1,
                LogicalRecordZ,
                total_credit_value=self._credits_total,
                total_credit_number=len(self._credits),
                total_debit_value=self._debits_total,
                total_debit_number=len(self._debits),
            )
        )

        for record in records:
            f.write(str(record))

    def _make_record(
        self, index: int, record_type: Type[LogicalRecordType], **kwargs: Any
    ) -> LogicalRecordType:
        return record_type(
            index=index,
            file_creation_no=self.file_creation_no,
            file_creation_date=self.file_creation_date,
            **self.originator_info.dict(),
            **kwargs,
        )
