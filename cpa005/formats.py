import enum
import re


class CurrencyCode(enum.Enum):
    CAD = "CAD"
    USD = "USD"

    def __str__(self):
        return self.value


class DollarAmount(int):
    """Field used to parse strings representing dollar amounts.
    To avoid innacuraccies introduced by floating-point calculations,
    the amount is contained in an integer, where the last to digits
    are the cents."""

    _regex = re.compile("[$]?(?P<dollars>[0-9]+)([.](?P<cents>[0-9][0-9])[0-9]*)?")

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v: str) -> "DollarAmount":
        m = cls._regex.fullmatch(v.strip())

        if m is None:
            raise ValueError(f"invalid dollar amount: {v}")

        dollars = m.group("dollars")

        cents = m.group("cents")
        if cents is None:
            cents = "00"

        return cls(f"{dollars}{cents}")
