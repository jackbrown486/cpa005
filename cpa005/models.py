import csv
from datetime import date

from pydantic import BaseModel, Field

from .formats import CurrencyCode, DollarAmount


class OriginatorInfo(BaseModel):
    class Config:
        use_enum_values = True

    currency_code_identifier: CurrencyCode = Field(
        default="CAD", alias="Currency Code Identifier"
    )
    destination_data_centre: int = Field(alias="Destination Data Centre")
    odc_settlement_code: str = Field(alias="ODC Settlement Code")
    odc_user_id: str = Field(alias="ODC User ID")
    originator_id: str = Field(alias="Originator ID")
    originator_short_name: str = Field(alias="Originator Short Name")
    originator_long_name: str = Field(alias="Originator Long Name")
    return_account_no: str = Field(alias="Return Account No")
    return_institution_no: int = Field(alias="Return Institution No")
    return_transit_no: int = Field(alias="Return Routing No")
    settlement_account_no: str = Field(alias="Settlement Account No")
    settlement_institution_no: int = Field(alias="Settlement Institution No")
    settlement_transit_no: int = Field(alias="Settlement Routing No")


class Transaction(BaseModel):

    amount: DollarAmount = Field(alias="Amount")
    cross_reference: str = Field(alias="Cross Reference")
    due_date: date = Field(alias="Due Date")
    transactor_name: str = Field(alias="Payor/Payee Name")
    institution_no: int = Field(alias="Institution No")
    transit_no: int = Field(alias="Routing No")
    account_no: str = Field(alias="Account No")
    transaction_type_no: int = Field(alias="Transaction Type")

    @classmethod
    def parse_csv(cls, data: str) -> list["Transaction"]:
        debits = []
        rows = csv.DictReader(data.splitlines())

        for row in rows:
            debits.append(cls(**row))

        return debits
