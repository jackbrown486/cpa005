"""
This module contains base classes for defining logical record element types and
transforming them into their output representation.

Concrete element types are created using the "class factory" pattern, using
functions to construct Element classes with the given parameters.
"""
import datetime
import time
from typing import Optional, Type, TypeVar

from . import Element

T = TypeVar("T")


class Literal(Element[str]):

    value: str

    def __init__(
        self,
        value: str,
        *,
        description: str = "",
        start_pos: int = 0,
    ):
        super().__init__(
            len(value),
            optional=True,
            description=description,
            start_pos=start_pos,
        )
        self.value = value

    def dumps(self, value: str) -> str:
        return value

    def loads(self, value: str) -> str:
        return value

    def init_value(self) -> str:
        return self.value

    def value_type(self) -> Type[str]:
        return str

    def validate_value(self, v: str) -> None:
        super().validate_value(v)

        if v != self.value:
            raise ValueError(self.name, v, f"Field must be {self.value}")


class Numeric(Element[int]):
    def __init__(
        self,
        width: int,
        *,
        description: str = "",
        max_value: Optional[int] = None,
        optional: bool = False,
        start_pos: int = 0,
    ):
        super().__init__(
            width, description=description, optional=optional, start_pos=start_pos
        )
        if max_value is None:
            self.max_value = 10**width
        else:
            self.max_value = max_value

    def dumps(self, value: int) -> str:
        """Return value in base-10 representation, padded with leading
        zeros to width."""

        return str(value).zfill(self.width)

    def loads(self, data: str) -> int:
        return int(data)

    def init_value(self) -> int:
        return 0

    def value_type(self) -> Type[int]:
        return int

    def validate_value(self, v: int) -> None:
        super().validate_value(v)

        if v >= self.max_value:
            raise ValueError(self.name, v, f"Field must be less than {self.max_value}")

        if v < 0:
            raise ValueError(self.name, v, "Field must be greater than or equal to 0")


class Alpha(Element[str]):
    def dumps(self, value: str) -> str:
        """Return value, right-justified with spaces to width."""
        return value.rjust(self.width)

    def loads(self, data: str) -> str:
        return data.lstrip()

    def init_value(self) -> str:
        return ""

    def value_type(self) -> Type[str]:
        return str

    def validate_value(self, v: str) -> None:
        super().validate_value(v)

        if len(v) > self.width:
            raise ValueError(
                self.name, v, f"Field must have length {self.width} or less."
            )


class Date(Element[datetime.date]):

    width = 6

    def __init__(
        self,
        *,
        description: str = "",
        optional: bool = False,
        start_pos: int = 0,
    ):
        super().__init__(
            6, description=description, optional=optional, start_pos=start_pos
        )

    def dumps(self, value: datetime.date) -> str:
        """If value is date.min, return a string of 6 zeros. Otherwise,
        return a date string in '0yyddd' format, where 'yy' is the last
        2 digits of the year, and 'ddd' is the (zero-padded) day number
        within the year (e.g Feb 2 is '033')."""

        if value == self.init_value:
            return f"{0:06}"

        time_tuple = value.timetuple()
        year_day = time_tuple.tm_yday
        year = str(time_tuple.tm_year)[-2:]

        return f"0{year}{year_day:03}"

    def loads(self, data: str) -> datetime.date:
        time_tuple = time.strptime(f"2{data}", "%Y%j")  # TODO: fix this for year 2100

        return datetime.date(year=time_tuple[0], month=time_tuple[1], day=time_tuple[2])

    def init_value(self) -> datetime.date:
        return datetime.date.min

    def value_type(self) -> Type[datetime.date]:
        return datetime.date


class InstitutionNo(Numeric):
    """3 digit bank number, with leading zero."""

    width = 4

    def __init__(
        self, *, description: str = "", optional: bool = False, start_pos: int = 0
    ):
        super().__init__(
            4,
            description=description,
            max_value=999,
            optional=optional,
            start_pos=start_pos,
        )
