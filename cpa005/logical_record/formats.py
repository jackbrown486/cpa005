def item_trace_no(
    destination_data_centre: int, file_creation_no: int, item_seq_no: int
) -> int:
    ddc = str(destination_data_centre)
    if len(ddc) != 5:
        raise ValueError("destination_data_center must be 5 digits")

    pdc = ddc[:-1]  # 'processing direct clearer'

    if len(str(file_creation_no)) > 4:
        raise ValueError("file_creation_no must be no longer than 4 digits")

    if len(str(item_seq_no)) > 9:
        raise ValueError("item_seq_no must be no longer than 9 digits")

    return int(f"{ddc}{pdc}{file_creation_no:04}{item_seq_no:09}")


def origination_control_data(
    originator_direct_clearer_user_id: str, file_seq_no: int
) -> str:
    if len(str(file_seq_no)) > 4:
        raise ValueError("file_seq_no must be no longer than 4 digits")

    return f"{originator_direct_clearer_user_id}{int(file_seq_no):04}"
