from __future__ import annotations

from abc import ABC, abstractmethod
from types import MappingProxyType
from typing import Any, ClassVar, Generic, Mapping, Optional, Type, TypeVar, overload

T = TypeVar("T")


class Element(ABC, Generic[T]):
    """
    Base class for logical record element, which is implemented using
    a descriptor.

    Attributes:
        description: description of this element
        name: attribute name of this element in its LogicalRecord. Initialized
            by __set_name__. May not start with an underscore.
        start_pos: the 0-based start position of this element in its LogicalRecord.
            If 0, will be initialized to the position immediately after the previous
            declared element by __set_name__.
        width: The character width of the output representation of this element
        value_type: The type of 'value'
    """

    SelfType = TypeVar("SelfType", bound="Element")

    description: str
    name: str
    optional: bool
    start_pos: int
    width: int

    def __init__(
        self,
        width: int,
        *,
        description: str = "",
        optional: bool = False,
        start_pos: int = 0,
    ):
        self.description = description
        self.optional = optional
        self.start_pos = start_pos
        self.width = width

    @overload
    def __get__(
        self: SelfType, instance: None, owner: Optional[Type["LogicalRecord"]]
    ) -> SelfType:
        ...

    @overload
    def __get__(
        self: SelfType, instance: LogicalRecord, owner: Optional[Type[LogicalRecord]]
    ) -> T:
        ...

    def __get__(self, instance, owner):
        if instance is not None:
            return instance._values[self.name]

        return self

    def __set__(self, instance: LogicalRecord, value: Optional[T]):
        if value is None:
            if not self.optional:
                raise ValueError(self.name, "Field is required.")

            value = self.init_value()

        self.validate_value(value)

        instance._values[self.name] = value

    def validate_value(self, v: T) -> None:
        """Called by __set__ with the candidate value. Raises a ValueError if v is
        invalid. Type checking is done by the base class."""

        if not isinstance(v, self.value_type()):
            raise TypeError(
                f"Element {self.name} expects value of type "
                f"{self.value_type().__name__}, got "
                f"{v} ({type(v).__name__})"
            )

    @abstractmethod
    def init_value(self) -> T:
        ...

    @abstractmethod
    def value_type(self) -> Type[T]:
        ...

    @abstractmethod
    def dumps(self, value: T) -> str:
        ...

    @abstractmethod
    def loads(self, data: str) -> T:
        ...


class LogicalRecord:
    """Base class for logical record representations, used for declarative
    specification of logical records. Inheriting classes should declare
    their elements in the class body in the order they appear in the
    specification.

    Attributes:
        elements: The Element attributes of this record, in the order they
            were declared
    """

    class Config:
        width = 1464

    elements: ClassVar[Mapping[str, Element]]

    def __init__(self, **kwargs: Any):
        self._values = {**self._init_values}
        errors: list[str] = []

        for name, elem in self.elements.items():
            val = kwargs.pop(name, None)

            try:
                setattr(self, name, val)
            except ValueError as exc:
                errors.append(str(exc))

        if errors:
            raise ValueError("\n".join(str(error) for error in errors))

    def __str__(self) -> str:
        """Convert this record to its output representation as a string. Output is a
        single-line string of length width.
        """
        acc = []
        pos = 0
        for name, elem in self.elements.items():
            if pos != elem.start_pos:
                acc.append(" " * (elem.start_pos - pos))
            acc.append(elem.dumps(getattr(self, name)))
            pos = elem.start_pos + elem.width

        return "".join(acc).ljust(self.Config.width)

    @classmethod
    def get_element(cls, name: str) -> Element:
        return getattr(cls, name)

    def __init_subclass__(cls) -> None:
        """Initializes the element_names attribute of a LogicalRecord and sets
        the name and start_pos attributes of its elements."""
        elements = {}
        kwargs = set()

        init_values = {}
        pos = 0
        for name, elem in cls.__dict__.items():
            if not isinstance(elem, Element):
                continue

            elem.name = name
            elements[name] = elem

            if elem.start_pos == 0:
                elem.start_pos = pos
                pos += elem.width
            elif elem.start_pos < pos:
                raise ValueError(
                    f"Element {name} has start_pos {elem.start_pos}"
                    "after the end of preceding element"
                )
            else:
                pos = elem.start_pos + elem.width

            init_values[elem.name] = elem.init_value()

        cls.elements = MappingProxyType(elements)
        cls._init_values = init_values
        cls._kwargs = kwargs

    _kwargs: ClassVar[set[str]]
    _init_values: ClassVar[dict[str, Any]]
