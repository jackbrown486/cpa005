import csv
import os
import tkinter
from datetime import date
from tkinter import filedialog
from typing import ClassVar, Optional, Sequence

import tkcalendar

from cpa005 import AFTFileWriter
from cpa005.models import OriginatorInfo, Transaction

ENCODING = "ascii"


class Stderr(tkinter.Toplevel):
    def __init__(self, root):
        self.txt = tkinter.Text(root)

    def write(self, s):
        self.txt.insert("insert", s)


class CSVToAFTGUI:

    button_width: ClassVar[int] = 30
    title: ClassVar[str] = "CSVtoAFT"

    file_creation_date_entry: tkcalendar.DateEntry
    file_creation_no_entry: tkinter.IntVar
    originator_info_other_entries: dict[str, tkinter.StringVar]
    originator_info_path_default: ClassVar[str] = "originator_info.csv"

    root: tkinter.Tk

    def __init__(self):
        self.root = tkinter.Tk()
        self.root.title(self.title)

        self.file_creation_date_entry = tkcalendar.DateEntry(self.root)
        self.file_creation_no_entry = tkinter.IntVar(self.root)

        self.originator_info_entries: dict[str, tkinter.StringVar] = {}
        for field in OriginatorInfo.__fields__.values():
            self.originator_info_entries[field.alias] = tkinter.StringVar(self.root)

        self.originator_info_path = tkinter.StringVar(self.root)

        self.make_gui()

    @property
    def originator_info(self) -> OriginatorInfo:
        return OriginatorInfo(
            **{name: var.get() for name, var in self.originator_info_entries.items()}
        )

    @property
    def file_creation_no(self) -> int:
        fcn = self.file_creation_no_entry.get()
        if not fcn > 0:
            raise ValueError("File Creation No cannot be 0")

        return fcn

    @property
    def file_creation_date(self) -> date:
        return self.file_creation_date_entry.get_date()

    def make_gui(self):
        row, column = 1, 1

        tkinter.Label(self.root, text="File Creation Date").grid(row=row, column=1)
        self.file_creation_date_entry.grid(row=row, column=column + 1)
        row += 1

        tkinter.Label(self.root, text="File Creation No").grid(row=row, column=1)
        tkinter.Entry(self.root, textvariable=self.file_creation_no_entry).grid(
            row=row, column=column + 1
        )
        row += 1

        row += 1
        row, column = self.make_originator_info_entry(row, column)

        row += 1

        tkinter.Button(
            self.root,
            text="Load Debits",
            command=lambda: self.write_aft_file(debits=self.load_transactions()),
        ).grid(row=row, column=column)

        tkinter.Button(
            self.root,
            text="Load Credits",
            command=lambda: self.write_aft_file(credits=self.load_transactions()),
        ).grid(row=row, column=column + 1)

        row += 1

        self.terminal = tkinter.Text(self.root, width=self.button_width * 2)
        self.terminal.grid(row=row, column=column, columnspan=2)

    def make_originator_info_entry(
        self, row_start: int, column_start: int
    ) -> tuple[int, int]:
        row, column = row_start, column_start

        row += 2
        tkinter.Label(self.root, text="Originator Info").grid(row=row, column=column)
        tkinter.Button(
            self.root,
            text="Load From File",
            command=lambda: self.load_originator_info(
                self.get_csv_path("Select Originator Info File")
            ),
        ).grid(row=row, column=column + 1)
        row += 1

        for name, strvar in self.originator_info_entries.items():
            tkinter.Label(self.root, text=name).grid(row=row, column=column)
            tkinter.Entry(self.root, textvariable=strvar).grid(
                row=row, column=column + 1
            )
            row += 1

        return row, column

    def write_aft_file(
        self,
        *,
        credits: Sequence[Transaction] = tuple(),
        debits: Sequence[Transaction] = tuple(),
    ):
        if not (credits or debits):
            return

        outpath = self.get_output_file_name()

        if outpath is not None:
            writer = AFTFileWriter(
                self.file_creation_no, self.file_creation_date, self.originator_info
            )
            writer.add_credits(credits)
            writer.add_debits(debits)

            with open(outpath, "w", encoding=ENCODING) as f:
                writer.write(f)

            self.write_originator_info(self.originator_info_path_default)

    def start(self):
        try:
            self.load_originator_info(self.originator_info_path_default)
        except FileNotFoundError:
            pass

        self.root.report_callback_exception = lambda et, ev, tb: self.handle_error(
            et, ev, tb
        )
        self.root.mainloop()

    def get_csv_path(self, title: str, **kwargs) -> Optional[str]:
        path = filedialog.askopenfilename(
            title=title, filetypes=[("CSV Files", "*.csv")], **kwargs
        )
        if not path:
            return None

        return path

    @property
    def default_output_file_name(self) -> str:
        originator_short_name = self.originator_info_entries[
            "Originator Short Name"
        ].get()

        return (
            f"{originator_short_name}-"
            f"{self.file_creation_no}-{self.file_creation_date.isoformat()}.aft.txt"
        )

    def get_output_file_name(self) -> Optional[str]:
        path = filedialog.asksaveasfilename(
            initialfile=self.default_output_file_name, initialdir=os.getcwd()
        )
        if not path:
            return None

        return path

    def load_transactions(self) -> list[Transaction]:
        path = self.get_csv_path("Load transactions from CSV")
        if path is None:
            return []

        transactions: list[Transaction] = []
        with open(path, "r", newline="") as f:
            reader = csv.DictReader(f)
            for row in reader:
                transactions.append(Transaction(**row))

        return transactions

    def load_originator_info(self, path: Optional[str]):
        if path is None:
            return

        with open(path, "r", newline="") as f:
            reader = csv.reader(f)
            for row in reader:
                self.originator_info_entries[row[0]].set(row[1])

    def write_originator_info(self, path: Optional[str]):
        if path is None:
            return

        with open(path, "w", newline="") as f:
            writer = csv.writer(f)
            for name, var in self.originator_info_entries.items():
                writer.writerow((name, var.get()))

    def handle_error(self, exc_type, exc_val, tb):
        self.terminal.insert("insert", f"Error: {str(exc_val)}\n")

    def trim_button_string(self, string):
        if len(string) > self.button_width:
            return f"...{string[-(self.button_width - 5):]}"


def main(*args, **kwargs):
    CSVToAFTGUI().start()


if __name__ == "__main__":
    main()
