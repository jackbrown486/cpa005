"""Contains specification of concrete LogicalRecord types."""
from cpa005.logical_record.formats import item_trace_no

from .logical_record import LogicalRecord
from .logical_record.elements import Alpha, Date, InstitutionNo, Literal, Numeric


class LogicalRecordA(LogicalRecord):
    """The A record must occur as the first Logical Record in an AFT file."""

    type_id = Literal("A")
    index = Numeric(9)
    originator_id = Alpha(10)
    file_creation_no = Numeric(4)
    file_creation_date = Date()
    destination_data_centre = Numeric(5)
    customer_direct_clearer_comm_area = Alpha(20, optional=True)
    currency_code_identifier = Alpha(3)
    # Fields below required for RBC?
    # mystery_sequence = Literal("000101", start_pos=1248)
    # settlement_institution_no = InstitutionNo()
    # settlement_transit_no = Numeric(5)
    # settlement_account_no = Alpha(7)


class LogicalRecordZ(LogicalRecord):
    """The Z record terminates an AFT file. It must be the final Logical Record in
    the file."""

    type_id = Literal("Z")
    index = Numeric(9)
    originator_id = Alpha(10)
    file_creation_no = Numeric(4)
    total_debit_value = Numeric(14, optional=True)
    total_debit_number = Numeric(8, optional=True)
    total_credit_value = Numeric(14, optional=True)
    total_credit_number = Numeric(8, optional=True)
    total_field_e_value = Numeric(14, optional=True)
    total_field_e_number = Numeric(8, optional=True)
    total_field_f_value = Numeric(14, optional=True)
    total_field_f_number = Numeric(8, optional=True)


class LogicalRecordC(LogicalRecord):
    """Logical Record for a credit transaction."""

    type_id = Literal("C")
    index = Numeric(9)
    originator_id = Alpha(10)
    file_creation_no = Numeric(4)
    transaction_type_no = Numeric(3)
    amount = Numeric(10, description="Transaction amount, in cents.")
    due_date = Date()
    institution_no = InstitutionNo()
    transit_no = Numeric(5)
    account_no = Alpha(12)
    item_trace_no = Numeric(22)
    stored_transaction_type_no = Numeric(3, optional=True)
    originator_short_name = Alpha(15)
    transactor_name = Alpha(30)
    originator_long_name = Alpha(30)
    odc_user_id = Alpha(10)
    cross_reference = Alpha(19, optional=True)
    return_institution_no = InstitutionNo()
    return_transit_no = Numeric(5)
    return_account_no = Alpha(12)
    originator_sundry_info = Alpha(15, optional=True)
    odc_settlement_code = Alpha(2, start_pos=251)
    invalid_data_element_id = Numeric(11, optional=True)

    def __init__(
        self, destination_data_centre: int, file_creation_no: int, index: int, **kwargs
    ):
        kwargs["item_trace_no"] = item_trace_no(
            destination_data_centre, file_creation_no, index
        )

        super().__init__(index=index, file_creation_no=file_creation_no, **kwargs)


class LogicalRecordD(LogicalRecord):
    """Logical Record for a debit transaction."""

    type_id = Literal("D")
    index = Numeric(9)
    originator_id = Alpha(10)
    file_creation_no = Numeric(4)
    transaction_type_no = Numeric(3)
    amount = Numeric(10, description="Amount of transaction, in cents.")
    due_date = Date()
    institution_no = InstitutionNo()
    transit_no = Numeric(5)
    account_no = Alpha(12)
    item_trace_no = Numeric(22)
    stored_transaction_type_no = Numeric(3, optional=True)
    originator_short_name = Alpha(15)
    transactor_name = Alpha(30)
    originator_long_name = Alpha(30)
    odc_user_id = Alpha(10)
    cross_reference = Alpha(19, optional=True)
    return_institution_no = InstitutionNo()
    return_transit_no = Numeric(5)
    return_account_no = Alpha(12)
    originator_sundry_info = Alpha(15, optional=True)
    odc_settlement_code = Alpha(2, start_pos=251)
    invalid_data_element_id = Numeric(11, optional=True)

    def __init__(
        self, destination_data_centre: int, file_creation_no: int, index: int, **kwargs
    ):
        kwargs["item_trace_no"] = item_trace_no(
            destination_data_centre, file_creation_no, index
        )

        super().__init__(index=index, file_creation_no=file_creation_no, **kwargs)
