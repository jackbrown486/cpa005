PYTHON_BIN=python3.9

.DEFAULT_GOAL:= help
.VENV:= .venv
.VENV_BIN:= $(.VENV)/bin


.PHONY:
help:
	@printf "Help?\n"


.PHONY: format
format: venv
	$(.VENV_BIN)/black . && $(.VENV_BIN)/isort .


format-check: venv
	$(.VENV_BIN)/black --check . && $(.VENV_BIN)/isort --check-only .


lint: venv
	$(.VENV_BIN)/flake8 cpa005/ tests/

.PHONY: test
test: venv
	$(.VENV)/bin/python -m pytest tests

.PHONY: clean
clean:
	rm -rf $(.VENV) cpa005.egg-info ./build ./dist

venv: $(.VENV)

# ------------------------------- #

dist/app: app/app.py app.spec cpa005/
	pyinstaller --onefile app.spec

$(.VENV): setup.cfg pyproject.toml
	@printf "Creating virtual environment at $(shell pwd)/$(.VENV)\n"
	$(PYTHON_BIN) -m venv $(.VENV)
	$(.VENV)/bin/pip --use-feature=in-tree-build install .[test]
