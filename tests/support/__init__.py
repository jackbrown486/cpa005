# flake8: noqa
import inspect

TRANSACTIONS_CSV = inspect.cleandoc(
    """
    Payor/Payee Name,Cross Reference,Institution No,Routing No,Account No,Amount,Due Date,Transaction Type
    "Ulyanov, Vladimir Ilyich",SUB 1917,222,22222,222222222,$1917.50,1921-01-01,470
    "Bronstein, Lev Davidovich",SUB 1905,333,33333,33333333,$1905,1921-01-01,470
"""
)

originator_info_CSV = inspect.cleandoc(
    """
    Currency Code Identifier,CAD,
    Destination Data Centre,12345,
    File Creation Date,2021-01-01,
    File Creation No,50,
    ODC Settlement Code,01,
    ODC User ID,123456789,
    Originator ID,ABC12345678,
    Originator Short Name,ISKRA,
    Originator Long Name,ISKRA,
    Return Account No,1234567,
    Return Institution No,123,
    Return Routing No,12345,
"""
)
