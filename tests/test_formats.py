import pytest

from cpa005.formats import DollarAmount


class TestDollarAmount:
    @pytest.mark.parametrize(
        "input,output",
        [
            ["123.50", 12350],
            ["$123.50", 12350],
            ["123", 12300],
            ["$123", 12300],
            ["123.507", 12350],
        ],
    )
    def test_valid(self, input, output):
        assert DollarAmount.validate(input) == output

    @pytest.mark.parametrize("input", ["-123", "-$123.50", "125.50.65"])
    def test_invalid(self, input):
        with pytest.raises(ValueError):
            DollarAmount.validate(input)
