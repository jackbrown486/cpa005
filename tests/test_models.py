from datetime import date

from cpa005.models import Transaction
from tests.support import TRANSACTIONS_CSV


class TestTransaction:
    def test_parse_csv(self):
        pads = Transaction.parse_csv(TRANSACTIONS_CSV)

        assert len(pads) == 2

        assert pads[0].transactor_name == "Ulyanov, Vladimir Ilyich"
        assert pads[0].cross_reference == "SUB 1917"
        assert pads[0].institution_no == 222
        assert pads[0].transit_no == 22222
        assert pads[0].account_no == "222222222"
        assert pads[0].amount == 191750
        assert pads[0].due_date == date(1921, 1, 1)
        assert pads[0].transaction_type_no == 470

        assert pads[1].transactor_name == "Bronstein, Lev Davidovich"
        assert pads[1].cross_reference == "SUB 1905"
        assert pads[1].amount == 190500
