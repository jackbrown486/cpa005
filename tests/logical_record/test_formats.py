import pytest

from cpa005.logical_record.formats import item_trace_no


def test_item_trace_no():
    assert item_trace_no(12345, 50, 1917) == 1234512340050000001917


@pytest.mark.parametrize(
    "ddc, fsn, isn", [[1234, 50, 1917], [12345, 50000, 1917], [12345, 50, 9999999999]]
)
def test_item_trace_no_invalid(ddc, fsn, isn):
    with pytest.raises(ValueError):
        item_trace_no(ddc, fsn, isn)
