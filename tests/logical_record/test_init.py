from datetime import date

import pytest

from cpa005.logical_record import Element, LogicalRecord
from cpa005.logical_record.elements import Alpha, Date, Literal, Numeric


class TestLogicalRecord:
    class MyRecord(LogicalRecord):
        elem_one = Literal("one")
        elem_two = Alpha(5)
        elem_three = Alpha(5, optional=True, start_pos=10)
        elem_four = Numeric(7)
        elem_five = Date(optional=True)

    def test_class_access_element(self):
        assert isinstance(self.MyRecord.elem_one, Element)

    def test_class_elements(self):
        """Test that the LogicalRecord subclass has an attribute
        element_names that is an tuple of the names of the elements
        as they are declared in the class body."""

        assert self.MyRecord.elements == {
            "elem_one": self.MyRecord.elem_one,
            "elem_two": self.MyRecord.elem_two,
            "elem_three": self.MyRecord.elem_three,
            "elem_four": self.MyRecord.elem_four,
            "elem_five": self.MyRecord.elem_five,
        }

    def test_init_missing_required(self):
        with pytest.raises(ValueError):
            self.MyRecord(elem_two="two", elem_three="three")

    def test_init(self):
        record = self.MyRecord(elem_two="two", elem_four=4, elem_five=date(2017, 11, 7))

        assert record.elem_one == "one"
        assert record.elem_two == "two"
        assert record.elem_three == ""
        assert record.elem_four == 4
        assert record.elem_five == date(2017, 11, 7)

    def test_init_literal_error(self):
        with pytest.raises(ValueError):
            self.MyRecord(
                elem_one="abc", elem_two="two", elem_three="three", elem_four=4
            )

    def test_str(self):
        record = self.MyRecord(elem_two="two", elem_four=4, elem_five=date(1917, 11, 7))

        expect = "one" + "  two" + "  " + "     " + "0000004" + "017311"

        check = str(record)
        filler = check[len(expect) :]

        assert len(check) == record.Config.width
        assert check.startswith(expect)
        assert filler.count(" ") == len(filler)

        for name, element in record.elements.items():
            val = getattr(record, name)
            element_dumped = check[
                element.start_pos : element.start_pos + element.width
            ]
            assert element_dumped.startswith(element.dumps(val))
            assert element_dumped[len(element.dumps(val)) :].count(
                " "
            ) == element.width - len(element.dumps(val))
